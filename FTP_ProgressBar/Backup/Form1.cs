using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace FTP_ProgressBar
{
	public partial class Form1 : Form
	{
		public Form1()
		{
			InitializeComponent();
		}

		private void Form1_Load(object sender, EventArgs e)
		{

		}

		private void btnBrowse_Click(object sender, EventArgs e)
		{
			if(this.openFileDialog1.ShowDialog() != DialogResult.Cancel)
				this.txtUploadFile.Text = this.openFileDialog1.FileName;
		}

		private void btnUpload_Click(object sender, EventArgs e)
		{
			// the upload button is also used as a cancel button, depending on the state of the FtpProgress thread
			if(this.ftpProgress1.IsBusy)
			{
				this.ftpProgress1.CancelAsync();
				this.btnUpload.Text = "Upload";
			}
			else
			{
				// create a new FtpSettings class to store all the paramaters for the FtpProgress thread
				FtpSettings f = new FtpSettings();
				f.Host = this.txtHost.Text;
				f.Username = this.txtUsername.Text;
				f.Password = this.txtPassword.Text;
				f.TargetFolder = this.txtDir.Text;
				f.SourceFile = this.txtUploadFile.Text;
				f.Passive = this.chkPassive.Checked;
				try
				{
					f.Port = Int32.Parse(this.txtPort.Text);
				}
				catch { }
				this.toolStripProgressBar1.Visible = true;
				this.ftpProgress1.RunWorkerAsync(f);
				this.btnUpload.Text = "Cancel";
			}
		}

		private void ftpProgress1_ProgressChanged(object sender, ProgressChangedEventArgs e)
		{
			this.toolStripStatusLabel1.Text = e.UserState.ToString();	// the message will be something like: 45 Kb / 102.12 Mb
			this.toolStripProgressBar1.Value = Math.Min(this.toolStripProgressBar1.Maximum, e.ProgressPercentage);		
		}

		private void ftpProgress1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
		{
			if(e.Error != null)
				MessageBox.Show(e.Error.ToString(), "FTP error");
			else if(e.Cancelled)
				this.toolStripStatusLabel1.Text = "Upload Cancelled";
			else
				this.toolStripStatusLabel1.Text = "Upload Complete";
			this.btnUpload.Text = "Upload";
			this.toolStripProgressBar1.Visible = false;
		}
	}
}