﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Trestto.Relatorios
{
    public class RelatorioDB
    {
        public string carteira { get; set; }

        public string tipo_fluxo { get; set; }

        public string lote { get; set; }

        public string cpf { get; set; }

        public string contrato { get; set; }

        public string telefone { get; set; }

        public string nomecliente { get; set; }

        public string estado { get; set; }

        public DateTime data { get; set; }

        public string hora { get; set; }

        public string status_telefonia { get; set; }

        public string opcao { get; set; }

        public string descricao_opcao { get; set; }

        public string qtdpc { get; set; }

        public string valor { get; set; }

        public string cpc { get; set; }

        public string novotelefone { get; set; }

        public string contactID { get; set; }
    }
}
