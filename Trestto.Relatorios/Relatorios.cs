﻿using FTP_ProgressBar;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Excel = Microsoft.Office.Interop.Excel;

namespace Trestto.Relatorios
{
    public class Relatorios
    {

        public void GeraRelatorio()
        {
            var msg = "";

            try
            {
                List<RelatorioDB> rRelatorio = new List<RelatorioDB>();

                SqlConnection sqlConnection1 = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["DASHBOARD"].ConnectionString);
                SqlCommand cmd = new SqlCommand();

                cmd.CommandText = "sp_RPT_NP_386_AcompanhamentoOperacao_V1";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = sqlConnection1;
                cmd.CommandTimeout = 280;
                sqlConnection1.Open();

                SqlDataReader dr = cmd.ExecuteReader();

                DataTable dt = new DataTable();

                dt.Load(dr);

                List<string> output = new List<string>();

                output.Add("carteira" + ";" + "tipo_fluxo" + ";" + "lote" + ";" + "cpf" + ";" + "contrato" + ";" + "telefone" + ";" + "nomecliente" + ";" + "estado" + ";" + "data" + ";" + "hora" + ";" + "status_telefonia" + ";" + "opcao" + ";" + "descricao_opcao" + ";" + "qtdpc" + ";" + "valor" + ";" + "cpc" + ";" + "novotelefone" + ";" + "contactID");


                foreach (DataRow item in dt.Rows)
                {
                    output.Add(item["carteira"] + ";" + item["tipo_fluxo"] + ";" + item["lote"] + ";" + item["cpf"] + ";" + item["contrato"] + ";" + item["telefone"] + ";" + item["nomecliente"] + ";" + item["estado"] + ";" + Convert.ToDateTime(item["data"]).ToString("yyyy-MM-dd") + "; " + item["hora"] + ";" + item["status_telefonia"] + ";" + item["opcao"] + ";" + item["descricao_opcao"] + ";" + item["qtdpc"] + ";" + item["valor"] + ";" + item["cpc"] + ";" + item["novotelefone"] + ";" + item["contactID"]);
                }

                sqlConnection1.Close();

                var path = System.IO.Path.GetDirectoryName(System.IO.Path.GetTempFileName());

                DateTime Data = DateTime.Now;

                var tey = Data.ToString("yyyyMMdd");

                string pathCsv = path + "\\relatório_retorno_base_bradesco_" + tey + ".csv";

                if (File.Exists(pathCsv))
                {
                    File.Delete(pathCsv);
                }

                System.IO.File.WriteAllLines(pathCsv, output.ToArray());

                System.Threading.Thread.Sleep(25000);


                FtpProgress ftp = new FtpProgress();

                FtpSettings f = new FtpSettings();
                f.Host = "ftp://cloud.vocalcom.com.br";
                f.Username = "trestto.ftp";
                f.Password = "057xg4sWRO";
                f.TargetFolder = "/RETORNO/";
                f.SourceFile = pathCsv;
                f.Passive = true;
                f.Port = 21;
                ftp.RunWorkerAsync(f);

                System.Threading.Thread.Sleep(20000);

                msg = "Arquivo enviado";
            }
            catch (Exception ex)
            {

                msg = ex.Message.ToString();
            }


            GravaLog(msg);
        }

        public string CriaExcel(List<RelatorioDB> rRelatorio)
        {
            Excel.Application excel = new Excel.Application();
            Excel.Workbook wb = excel.Workbooks.Add(Excel.XlWBATemplate.xlWBATWorksheet);
            Excel.Worksheet ws = (Excel.Worksheet)wb.Worksheets.get_Item(1);

            ws.Name = "Relatorio_13";
            ws.Columns.AutoFit();

            for (int i = 0; i < rRelatorio.Count; i++)
            {
                Microsoft.Office.Interop.Excel.Range ce = (ws.Cells[i + 1, 1] as Microsoft.Office.Interop.Excel.Range);
                Microsoft.Office.Interop.Excel.Range ce2 = (ws.Cells[i + 1, 2] as Microsoft.Office.Interop.Excel.Range);
                int linha = i + 1;
                ce.Value2 = rRelatorio[i].carteira;
                ce2.Value2 = rRelatorio[i].tipo_fluxo;
            }

            var path = @"D:\Relatorios\";

            var x = DateTime.Now.ToShortDateString().Replace("-", string.Empty).Replace("\\", string.Empty).Replace("/", string.Empty);

            string pathCsv = path + "Relatorio" + x + ".xlsx";

            wb.SaveAs(pathCsv, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlNoChange, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);

            excel.Quit();

            return pathCsv;
        }

        public void GravaLog(string msg)
        {
            DateTime Data = DateTime.Now;

            var tey = Data.ToString("yyyyMMdd");

            string pathCsv = "ftp://cloud.vocalcom.com.br/RETORNO/relatório_retorno_base_bradesco_" + tey + ".csv";

            SqlConnection sqlConnection1 = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["DASHBOARD"].ConnectionString);
            SqlCommand cmd = new SqlCommand();

            cmd.CommandText = "sp_OPTIMUS_REGISTRA_EVENTO";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = sqlConnection1;

            cmd.Parameters.Add("@Campanha", SqlDbType.VarChar).Value = "3860";
            cmd.Parameters.Add("@idCliente", SqlDbType.Int).Value = DBNull.Value;
            cmd.Parameters.Add("@idLista", SqlDbType.VarChar).Value = DBNull.Value;
            cmd.Parameters.Add("@idEvento", SqlDbType.Int).Value = 10;
            cmd.Parameters.Add("@TipoExecucao", SqlDbType.VarChar).Value = "ROBO";
            cmd.Parameters.Add("@Comando", SqlDbType.VarChar).Value = pathCsv;
            cmd.Parameters.Add("@CodAgente", SqlDbType.VarChar).Value = DBNull.Value;

            if (msg == "Arquivo enviado")
            {
                cmd.Parameters.Add("@Status", SqlDbType.VarChar).Value = "OK";
            }
            else
            {
                cmd.Parameters.Add("@Status", SqlDbType.VarChar).Value = "ERRO";
            }

            cmd.Parameters.Add("@Mensagem", SqlDbType.VarChar).Value = msg;
            cmd.Parameters.Add("@DataInicioEvento", SqlDbType.DateTime).Value = DateTime.Now;
            cmd.Parameters.Add("@DataTerminoEvento", SqlDbType.DateTime).Value = DateTime.Now;
            cmd.Parameters.Add("@IdUsuario", SqlDbType.Int).Value = 0;
            sqlConnection1.Open();

            SqlDataReader dr = cmd.ExecuteReader();
        }

        public void GeraLogExecucao()
        {

            SqlConnection sqlConnection1 = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["DASHBOARD"].ConnectionString);
            SqlCommand cmd = new SqlCommand();

            cmd.CommandText = "sp_OPTIMUS_REGISTRA_EVENTO";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = sqlConnection1;

            cmd.Parameters.Add("@Campanha", SqlDbType.VarChar).Value = "3860";
            cmd.Parameters.Add("@idCliente", SqlDbType.Int).Value = DBNull.Value;
            cmd.Parameters.Add("@idLista", SqlDbType.VarChar).Value = DBNull.Value;
            cmd.Parameters.Add("@idEvento", SqlDbType.Int).Value = 10;
            cmd.Parameters.Add("@TipoExecucao", SqlDbType.VarChar).Value = "ROBO";
            cmd.Parameters.Add("@Comando", SqlDbType.VarChar).Value = "Em andamento";
            cmd.Parameters.Add("@CodAgente", SqlDbType.VarChar).Value = DBNull.Value;

                cmd.Parameters.Add("@Status", SqlDbType.VarChar).Value = "OK";

            cmd.Parameters.Add("@Mensagem", SqlDbType.VarChar).Value = "Job em Execução";
            cmd.Parameters.Add("@DataInicioEvento", SqlDbType.DateTime).Value = DateTime.Now;
            cmd.Parameters.Add("@DataTerminoEvento", SqlDbType.DateTime).Value = DateTime.Now;
            cmd.Parameters.Add("@IdUsuario", SqlDbType.Int).Value = 0;
            sqlConnection1.Open();

            SqlDataReader dr = cmd.ExecuteReader();
        }

    }



}
