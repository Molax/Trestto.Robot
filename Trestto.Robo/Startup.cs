﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Trestto.Robo.Startup))]
namespace Trestto.Robo
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
