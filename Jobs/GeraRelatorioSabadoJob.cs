﻿using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Trestto.Relatorios;

namespace Jobs
{
    public class GeraRelatorioSabadoJob : IJob
    {
        public void Execute(IJobExecutionContext context)
        {
            Relatorios relatorio = new Relatorios();
            relatorio.GeraRelatorio();
        }
    }
}
