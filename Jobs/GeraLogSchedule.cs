﻿using Quartz;
using Quartz.Impl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Trestto.Relatorios;

namespace Jobs
{
    public class GeraLogSchedule
    {
        public static void Start()
        {
            IScheduler scheduler = StdSchedulerFactory.GetDefaultScheduler();
            scheduler.Start();

            IJobDetail job = JobBuilder.Create<GeraLogJob>().Build();


            ITrigger trigger = TriggerBuilder.Create()
                .WithDailyTimeIntervalSchedule
                (s => s.WithIntervalInMinutes(5)
                .OnEveryDay()
                .StartingDailyAt(TimeOfDay.HourAndMinuteOfDay(7, 0))
                .EndingDailyAt(TimeOfDay.HourAndMinuteOfDay(20, 0)))
                .Build();

            scheduler.ScheduleJob(job, trigger);
        }
    }
}
