﻿using Quartz;
using Quartz.Impl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jobs
{
    public class GeraRelatorioScheduleSabado
    {
        public static void Start()
        {
            IScheduler scheduler = StdSchedulerFactory.GetDefaultScheduler();
            scheduler.Start();

            IJobDetail job = JobBuilder.Create<GeraRelatorioJob>().Build();


            ITrigger trigger = TriggerBuilder.Create()
                .WithDailyTimeIntervalSchedule
                (s => s.WithIntervalInMinutes(25)
                .OnSaturdayAndSunday()
                .StartingDailyAt(TimeOfDay.HourAndMinuteOfDay(6, 40))
                .EndingDailyAt(TimeOfDay.HourAndMinuteOfDay(14, 30)))
                .Build();

            scheduler.ScheduleJob(job, trigger);
        }
    }
}
