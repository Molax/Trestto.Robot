﻿using Quartz;
using Quartz.Impl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Trestto.Relatorios;
namespace Jobs
{
    public class GerarRelatorioSchedule
    {
        public static void Start()
        {
            IScheduler scheduler = StdSchedulerFactory.GetDefaultScheduler();
            scheduler.Start();

            IJobDetail job = JobBuilder.Create<GeraRelatorioJob>().Build();


            ITrigger trigger = TriggerBuilder.Create()
                .WithDailyTimeIntervalSchedule
                (s => s.WithIntervalInMinutes(25)
                .OnMondayThroughFriday()
                .StartingDailyAt(TimeOfDay.HourAndMinuteOfDay(6, 50))
                .EndingDailyAt(TimeOfDay.HourAndMinuteOfDay(21, 30)))
                .Build();

            scheduler.ScheduleJob(job, trigger);
        }
    }
}
